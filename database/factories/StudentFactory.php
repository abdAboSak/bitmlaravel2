<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'StudentID' => $faker->randomNumber(8, true),
        'studentName'=> $faker->name(),
        'FathersName'=> $faker->name(),
        'MothersName'=> $faker->name(),
        'class'=> $faker->randomElement(['One', 'Two', 'Three']),
        'DOB' => $faker->date()
    ];
});