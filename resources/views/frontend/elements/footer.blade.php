<!--::footer_part start::-->
<footer class="footer_part">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-sm-6 col-lg-2">
                <div class="single_footer_part">
                    <h4>Category</h4>
                    <ul class="list-unstyled">
                        <li><a href="{{asset('frontend/')}}#">Male</a></li>
                        <li><a href="{{asset('frontend/')}}#">Female</a></li>
                        <li><a href="{{asset('frontend/')}}#">Shoes</a></li>
                        <li><a href="{{asset('frontend/')}}#">Fashion</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-lg-2">
                <div class="single_footer_part">
                    <h4>Company</h4>
                    <ul class="list-unstyled">
                        <li><a href="{{asset('frontend/')}}">About</a></li>
                        <li><a href="{{asset('frontend/')}}">News</a></li>
                        <li><a href="{{asset('frontend/')}}">FAQ</a></li>
                        <li><a href="{{asset('frontend/')}}">Contact</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="single_footer_part">
                    <h4>Address</h4>
                    <ul class="list-unstyled">
                        <li><a href="{{asset('frontend/')}}#">200, Green block, NewYork</a></li>
                        <li><a href="{{asset('frontend/')}}#">+10 456 267 1678</a></li>
                        <li><span>contact89@winter.com</span></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="single_footer_part">
                    <h4>Newsletter</h4>
                    <div id="mc_embed_signup">
                        <form target="_blank"
                              action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                              method="get" class="subscribe_form relative mail_part">
                            <input type="email" name="email" id="newsletter-form-email" placeholder="Email Address"
                                   class="placeholder hide-on-focus" onfocus="this.placeholder = ''"
                                   onblur="this.placeholder = ' Email Address '">
                            <button type="submit" name="submit" id="newsletter-submit"
                                    class="email_icon newsletter-submit button-contactForm">subscribe</button>
                            <div class="mt-10 info"></div>
                        </form>
                    </div>
                    <div class="social_icon">
                        <a href="{{asset('frontend/')}}#"><i class="ti-facebook"></i></a>
                        <a href="{{asset('frontend/')}}#"><i class="ti-twitter-alt"></i></a>
                        <a href="{{asset('frontend/')}}#"><i class="ti-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="copyright_text">
                    <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="{{asset('frontend/')}}https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--::footer_part end::-->

<!-- jquery plugins here-->
<script src="{{asset('frontend/')}}/js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="{{asset('frontend/')}}/js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="{{asset('frontend/')}}/js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="{{asset('frontend/')}}/js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="{{asset('frontend/')}}/js/swiper.min.js"></script>
<!-- swiper js -->
<script src="{{asset('frontend/')}}/js/mixitup.min.js"></script>
<!-- particles js -->
<script src="{{asset('frontend/')}}/js/owl.carousel.min.js"></script>
<script src="{{asset('frontend/')}}/js/jquery.nice-select.min.js"></script>
<!-- slick js -->
<script src="{{asset('frontend/')}}/js/slick.min.js"></script>
<script src="{{asset('frontend/')}}/js/jquery.counterup.min.js"></script>
<script src="{{asset('frontend/')}}/js/waypoints.min.js"></script>
<script src="{{asset('frontend/')}}/js/contact.js"></script>
<script src="{{asset('frontend/')}}/js/jquery.ajaxchimp.min.js"></script>
<script src="{{asset('frontend/')}}/js/jquery.form.js"></script>
<script src="{{asset('frontend/')}}/js/jquery.validate.min.js"></script>
<script src="{{asset('frontend/')}}/js/mail-script.js"></script>
<!-- custom js -->
<script src="{{asset('frontend/')}}/js/custom.js"></script>
</body>

</html>